
// Your project must contain an AppConfig.h file with your project-specific settings in it,
// and your header search path must make it accessible to the module's files.
#include "AppConfig.h"

#include "juce_arg_parse.h"

namespace juce
{
    #include "argpare/juce_Argument.cpp"
    #include "argpare/juce_ArgumentGroup.cpp"
    #include "argpare/juce_ArgumentParser.cpp"
    #include "help_formatters/juce_HelpFormatter.cpp"
    #include "help_formatters/juce_RawDescriptionHelpFormatter.cpp"
    #include "help_formatters/juce_RawTextHelpFormatter.cpp"
    #include "help_formatters/juce_ArgumentDefaultsHelpFormatter.cpp"
}
