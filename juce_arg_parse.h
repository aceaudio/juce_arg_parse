
#ifndef JUCE_ARG_PARSE_H_INCLUDED
#define JUCE_ARG_PARSE_H_INCLUDED

#include "../juce_core/juce_core.h"

namespace juce
{
    #include "argpare/juce_Argument.h"
    #include "argpare/juce_ArgumentGroup.h"
    #include "argpare/juce_ArgumentParser.h"
    #include "help_formatters/juce_HelpFormatter.h"
    #include "help_formatters/juce_RawDescriptionHelpFormatter.h"
    #include "help_formatters/juce_RawTextHelpFormatter.h"
    #include "help_formatters/juce_ArgumentDefaultsHelpFormatter.h"
}

#endif // JUCE_ARG_PARSE_H_INCLUDED
